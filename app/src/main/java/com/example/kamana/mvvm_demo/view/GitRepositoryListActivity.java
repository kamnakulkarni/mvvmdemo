package com.example.kamana.mvvm_demo.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.InputMethodManager;

import com.example.kamana.mvvm_demo.GithubService;
import com.example.kamana.mvvm_demo.R;
import com.example.kamana.mvvm_demo.RepositoryAdapter;
import com.example.kamana.mvvm_demo.databinding.GitRepoListActivityBinding;
import com.example.kamana.mvvm_demo.modal.Repository;
import com.example.kamana.mvvm_demo.viewmodal.GitRepositoryListViewModal;

import java.util.List;

/**
 * Created by kamana on 17/7/17.
 */

public class GitRepositoryListActivity extends AppCompatActivity implements GitRepositoryListViewModal.DataListener{

    private GitRepoListActivityBinding binding;
    private GitRepositoryListViewModal modal;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.git_repo_list_activity);
        modal = new GitRepositoryListViewModal(this, this);
        binding.setGitViewModal(modal);

        setUpRecyclerView(binding.reposRecyclerView);

    }

    private void setUpRecyclerView(RecyclerView reposRecyclerView) {
        RepositoryAdapter adapter = new RepositoryAdapter();
        reposRecyclerView.setAdapter(adapter);
        reposRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onRepositoryChanged(List<Repository> repository) {
        RepositoryAdapter adapter = (RepositoryAdapter)binding.reposRecyclerView.getAdapter();
        adapter.setRepositories(repository);
        adapter.notifyDataSetChanged();
        hideSoftKeyBoard();
    }

    public void hideSoftKeyBoard(){
        InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(binding.editTextUsername.getWindowToken(),0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        modal.destroy();
    }
}
