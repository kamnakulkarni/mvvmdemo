package com.example.kamana.mvvm_demo.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.kamana.mvvm_demo.R;
import com.example.kamana.mvvm_demo.databinding.RepositoryActivityBinding;
import com.example.kamana.mvvm_demo.modal.Repository;
import com.example.kamana.mvvm_demo.viewmodal.RepositoryViewModal;
import com.example.kamana.mvvm_demo.viewmodal.ViewModal;

/**
 * Created by kamana on 18/7/17.
 */

public class RepositoryActivity extends AppCompatActivity {

    private static final String EXTRA_REPOSITORY = "EXTRA_REPOSITORY";

    private RepositoryActivityBinding binding;
    private RepositoryViewModal viewModal;

    public static Intent newIntent(Context context, Repository repository){
        Intent intent = new Intent(context, RepositoryActivity.class);
        intent.putExtra(EXTRA_REPOSITORY, repository);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.repository_activity);

        Repository repository = getIntent().getParcelableExtra(EXTRA_REPOSITORY);
        viewModal = new RepositoryViewModal(this,repository);
        binding.setRepositoryViewModal(viewModal);

        setTitle(repository.name);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModal.destroy();
    }
}
