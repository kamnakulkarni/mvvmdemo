package com.example.kamana.mvvm_demo;

import android.content.Context;
import android.content.SharedPreferences;

import java.security.PublicKey;

/**
 * Created by kamana on 14/7/17.
 */

public class Preference {

    static Preference instance;
    protected Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor mEditor;

    public static synchronized Preference getInstance(Context context){

        if(instance == null){
            instance = new Preference(context.getApplicationContext());
        }

        return instance;
    }

    public Preference(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(IPreference.APP_PREFERENCE_KEY,Context.MODE_PRIVATE);
        mEditor = sharedPreferences.edit();
    }

    private void setStringValue(String key, String value){
        mEditor.putString(key, value);
        mEditor.commit();
    }

    private String getStringValue(String key){
        return sharedPreferences.getString(key, "");
    }

    public void setUsername(String key, String value){
        setStringValue(key, value);
    }

    public String getUsername(String key){
        return getStringValue(key);
    }

    public void setPassword(String key, String value){
        setStringValue(key, value);
    }

    public String getPassword(String key){
       return getStringValue(key);
    }

    //clear all the preference stored
    public void clearAll(){
        mEditor.clear().commit();
    }
}
