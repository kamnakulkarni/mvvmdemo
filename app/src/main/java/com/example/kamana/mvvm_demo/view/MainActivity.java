package com.example.kamana.mvvm_demo.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.kamana.mvvm_demo.R;
import com.example.kamana.mvvm_demo.databinding.MainActivityBinding;
import com.example.kamana.mvvm_demo.viewmodal.MainViewModal;


public class MainActivity extends AppCompatActivity {

    private MainViewModal mainViewModal;
    private MainActivityBinding mainUsingDbBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //initializing data binding
        mainUsingDbBinding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        mainViewModal = new MainViewModal(this);
        mainUsingDbBinding.setMainView(mainViewModal);


    }

    public static Intent openNewActivity(Context context){
        Intent intent = new Intent(context, GitRepositoryListActivity.class);
        return intent;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainViewModal.destroy();
    }
}
