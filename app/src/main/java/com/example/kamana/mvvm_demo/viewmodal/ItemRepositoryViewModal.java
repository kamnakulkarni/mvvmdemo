package com.example.kamana.mvvm_demo.viewmodal;

import android.content.ClipData;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kamana.mvvm_demo.R;
import com.example.kamana.mvvm_demo.modal.Repository;
import com.example.kamana.mvvm_demo.view.RepositoryActivity;

import okhttp3.ResponseBody;

/**
 * Created by kamana on 17/7/17.
 */

public class ItemRepositoryViewModal extends BaseObservable implements ViewModal {

    private Repository repository;
    private Context context;

    public ItemRepositoryViewModal(Context context, Repository repository){
        this.context = context;
        this.repository = repository;
    }

    public String getName(){
        return repository.name;
    }

    public String getDescription(){
        return repository.description;
    }

    public String getStars(){
        return context.getString(R.string.text_stars, repository.stars);
    }

    public String getWatchers() {
        return context.getString(R.string.text_watchers, repository.watchers);
    }

    public String getForks() {
        return context.getString(R.string.text_forks, repository.forks);
    }

    public void onItemClick(View view){
        Toast.makeText(context, "item has been clicked", Toast.LENGTH_SHORT).show();
        context.startActivity(RepositoryActivity.newIntent(context, repository));
    }

    @Override
    public void destroy() {

    }

    public void setRepository(Repository repository){
        this.repository =  repository;
        notifyChange();
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName){
        textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(),"Fonts/"+fontName));
    }


}
