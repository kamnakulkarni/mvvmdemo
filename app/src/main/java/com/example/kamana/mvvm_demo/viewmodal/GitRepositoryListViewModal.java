package com.example.kamana.mvvm_demo.viewmodal;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kamana.mvvm_demo.GithubService;
import com.example.kamana.mvvm_demo.MvvmApplication;
import com.example.kamana.mvvm_demo.modal.Repository;

import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kamana on 17/7/17.
 */

public class GitRepositoryListViewModal implements ViewModal {

    private static final String APP_TAG = "GitRepoListViewModal";

    public ObservableInt searchButtonVisibility;
    public ObservableInt progressVisibility;
    public ObservableInt recyclerViewVisibility;

    private Context context;
    private Subscription subscription;
    private List<Repository> repositories;
    private DataListener listener;
    private String editTextUsernameValue;

    public GitRepositoryListViewModal(Context context, DataListener listener){
        this.listener = listener;
        this.context = context;

        searchButtonVisibility = new ObservableInt(View.GONE);
        progressVisibility = new ObservableInt(View.INVISIBLE);
        recyclerViewVisibility = new ObservableInt(View.INVISIBLE);
    }

    public TextWatcher getGithubUsername(){

        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTextUsernameValue = s.toString();
                searchButtonVisibility.set( editTextUsernameValue.length() > 0 ? View.VISIBLE : View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    public boolean onSearchAction(TextView view, int actionId, KeyEvent event){
        if(actionId == EditorInfo.IME_ACTION_SEARCH){
            String username = view.getText().toString();
            if(username.length() > 0){
                loadGithubRepositories(username);
            }
            return true;
        }

        return false;
    }

    private void loadGithubRepositories(String username) {
        progressVisibility.set(View.VISIBLE);
        recyclerViewVisibility.set(View.INVISIBLE);

        if(subscription!=null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }

        MvvmApplication mvvmApplication = MvvmApplication.get(context);
        GithubService githubService = mvvmApplication.getGithubService();
        subscription = githubService.publicRepositories(username)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(mvvmApplication.defaultSubscribeScheduler())
                        .subscribe(new Subscriber<List<Repository>>() {
                            @Override
                            public void onCompleted() {
                                if(listener != null)listener.onRepositoryChanged(repositories);

                                progressVisibility.set(View.INVISIBLE);
                                if(!repositories.isEmpty()){
                                    recyclerViewVisibility.set(View.VISIBLE);
                                } else {
                                    Toast.makeText(context, "No repo for particular username", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                progressVisibility.set(View.INVISIBLE);
                                if(isHttp404Error(e)){
                                    Toast.makeText(context, "Oops! the username is unknown", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(context, "Oops! Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onNext(List<Repository> repositories) {
                                GitRepositoryListViewModal.this.repositories = repositories;
                            }
                        });
    }



    public interface DataListener {
        void onRepositoryChanged(List<Repository> repository);
    }

    public void onClickSearch(View view){
        loadGithubRepositories(editTextUsernameValue);
    }

    @Override
    public void destroy() {

        if(subscription!=null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();

            subscription = null;
            context = null;
            listener = null;
        }
    }

    private static boolean isHttp404Error(Throwable e){
        return e instanceof HttpException && ((HttpException)e).code() == 404;
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(EditText editText, String fontName){
        editText.setTypeface(Typeface.createFromAsset(editText.getContext().getAssets(), "Fonts/"+fontName));
    }
}
