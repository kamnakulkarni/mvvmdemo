package com.example.kamana.mvvm_demo.viewmodal;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kamana.mvvm_demo.IPreference;
import com.example.kamana.mvvm_demo.Preference;
import com.example.kamana.mvvm_demo.view.MainActivity;


/**
 * Created by kamana on 12/7/17.
 */

public class MainViewModal implements ViewModal{

    Context context;
    public ObservableField<String> buttonText;
    public ObservableInt loginButtonVisibility;
    public ObservableInt progressBarVisibility;
    String editTextUsernameValue, editTextPasswordValue;

    Preference preference;

    public MainViewModal(Context context){
        this.context = context;
        buttonText = new ObservableField<>("Login Text");
        loginButtonVisibility = new ObservableInt(View.INVISIBLE);
        progressBarVisibility = new ObservableInt(View.INVISIBLE);
    }

    public TextWatcher getUsernameTextWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSeq, int start, int before, int count) {
                editTextUsernameValue = charSeq.toString();
                loginButtonVisibility.set(editTextUsernameValue.length() > 0 ? View.VISIBLE : View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    public TextWatcher getPasswordTextWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editTextPasswordValue = s.toString();
                if(editTextPasswordValue.length() > 4 ? true : false){

                }else {
                    Toast.makeText(context, "Password should be min 4 char long", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    public void onClickLogin(View view){
        saveCredential();
        performLogin();
    }

    private void saveCredential() {
        Preference.getInstance(context);
        preference = new Preference(context);
        preference.setUsername(IPreference.KEY_PREFERENCE_USERNAME , editTextUsernameValue);
        preference.setPassword(IPreference.KEY_PREFERENCE_PASSWORD , editTextPasswordValue);
    }

    private void performLogin() {
        progressBarVisibility.set(View.VISIBLE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBarVisibility.set(View.INVISIBLE);
                Toast.makeText(context, "Login Successful", Toast.LENGTH_SHORT).show();

                context.startActivity(MainActivity.openNewActivity(context));
            }
        }, 2000);
    }

    @Override
    public void destroy() {
        context = null;
    }

    @BindingAdapter({"bind:font"})
    public static void setFontStyle(EditText  edittext, String fontName){
        edittext.setTypeface(Typeface.createFromAsset(edittext.getContext().getAssets(), "Fonts/"+fontName));
    }
}
