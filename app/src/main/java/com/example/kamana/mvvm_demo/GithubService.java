package com.example.kamana.mvvm_demo;

import android.databinding.ObservableInt;

import com.example.kamana.mvvm_demo.modal.Repository;
import com.example.kamana.mvvm_demo.modal.User;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by kamana on 14/7/17.
 */

public interface GithubService {

    @GET("users/{username}/repos")
    Observable<List<Repository>> publicRepositories(@Path("username") String username);

    @GET
    Observable<User> userFromUrl(@Url String userUrl);

    class Factory{
        public static GithubService create(){
            Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("https://api.github.com/")
                                .addConverterFactory(GsonConverterFactory.create())
                                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                                .build();

            return retrofit.create(GithubService.class);
        }
    }
}
