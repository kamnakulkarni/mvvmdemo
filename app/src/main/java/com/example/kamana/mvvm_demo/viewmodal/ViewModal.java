package com.example.kamana.mvvm_demo.viewmodal;

/**
 * Created by kamana on 12/7/17.
 */

public interface ViewModal {

    public void destroy();
}
