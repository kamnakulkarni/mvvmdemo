package com.example.kamana.mvvm_demo.viewmodal;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.databinding.adapters.ImageViewBindingAdapter;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.kamana.mvvm_demo.GithubService;
import com.example.kamana.mvvm_demo.MvvmApplication;
import com.example.kamana.mvvm_demo.R;
import com.example.kamana.mvvm_demo.modal.Repository;
import com.example.kamana.mvvm_demo.modal.User;
import com.example.kamana.mvvm_demo.viewmodal.ViewModal;
import com.squareup.picasso.Picasso;

import java.util.StringTokenizer;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by kamana on 18/7/17.
 */

public class RepositoryViewModal implements ViewModal{

    public static final String TAG = "RepositoryViewModal";

    private Repository repository;
    private Context context;
    private Subscription subscription;

    public ObservableField<String> ownerName;
    public ObservableField<String> ownerEmail;
    public ObservableField<String> ownerLocation;

    public ObservableInt ownerLayoutVisibility;
    public ObservableInt ownerEmailVisibility;
    public ObservableInt ownerLocationVisibility;



    public RepositoryViewModal(Context context, final Repository repository){

        this.repository = repository;
        this.context = context;

        this.ownerName = new ObservableField<>();
        this.ownerEmail = new ObservableField<>();
        this.ownerLocation = new ObservableField<>();

        this.ownerLayoutVisibility = new ObservableInt(View.INVISIBLE);
        this.ownerEmailVisibility = new ObservableInt(View.VISIBLE);
        this.ownerLocationVisibility = new ObservableInt(View.VISIBLE);

        loadFullUser(repository.owner.url);
    }

    private void loadFullUser(String url) {
        MvvmApplication application = MvvmApplication.get(context);
        GithubService githubService = application.getGithubService();

        subscription = githubService.userFromUrl(url)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(application.defaultSubscribeScheduler())
                        .subscribe(new Action1<User>() {
                            @Override
                            public void call(User user) {
                                Log.i(TAG, "Full user data loaded "+ user);
                                ownerName.set(user.name);
                                ownerEmail.set(user.email);
                                ownerLocation.set(user.location);
                                ownerLayoutVisibility.set(View.VISIBLE);
                                ownerEmailVisibility.set(user.hasEmail() ? View.VISIBLE : View.GONE);
                                ownerLocationVisibility.set(user.hasLocation() ? View.VISIBLE : View.GONE);
                            }
                        });
    }

    @Override
    public void destroy() {

        if(subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
        context = null;
    }

    public String getDescription(){
        return repository.description;
    }

    public String getHomepage(){
        return repository.homepage;
    }

    public int getHomepageVisibility() {
        return repository.hasHomepage() ? View.VISIBLE : View.GONE;
    }

    public String getLanguage(){
        return context.getString(R.string.text_language, repository.language);
    }

    public int getLanguageVisibility(){
        return repository.hasLanguage() ? View.VISIBLE : View.GONE;
    }

    public int getForkVisibility(){
        return repository.isFork() ? View.VISIBLE : View.GONE;
    }

    public String getOwnerAvatarUrl(){
        return repository.owner.avatarUrl;
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl){
        Picasso.with(view.getContext())
                .load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .into(view);
    }
}
