package com.example.kamana.mvvm_demo;

/**
 * Created by kamana on 14/7/17.
 */

public interface IPreference {
    public static final String APP_PREFERENCE_KEY = "APP_PREFERENCE_KEY";
    public static final String KEY_PREFERENCE_USERNAME = "PREFERENCE USERNAME";
    public static final String KEY_PREFERENCE_PASSWORD = "PREFERENCE PASSWORD";
}
