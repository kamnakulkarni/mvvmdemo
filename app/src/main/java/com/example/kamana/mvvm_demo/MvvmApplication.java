package com.example.kamana.mvvm_demo;

import android.app.Application;
import android.content.Context;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by kamana on 14/7/17.
 */

public class MvvmApplication extends Application{

    private GithubService githubService;
    private Scheduler defaultSubscribeScheduler;

    public static MvvmApplication get(Context context){
        return (MvvmApplication) context.getApplicationContext();
    }

    public GithubService getGithubService(){
        if(githubService == null){
            githubService = GithubService.Factory.create();
        }

        return githubService;
    }

    public void setGithubService(GithubService githubService){
        this.githubService = githubService;
    }

    public Scheduler defaultSubscribeScheduler(){
        if(defaultSubscribeScheduler == null){
            defaultSubscribeScheduler = Schedulers.io();
        }

        return defaultSubscribeScheduler;
    }

    public void setDefaultSubscribeScheduler(Scheduler scheduler){
        this.defaultSubscribeScheduler = scheduler;
    }
}
