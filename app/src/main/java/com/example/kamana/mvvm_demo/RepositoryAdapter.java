package com.example.kamana.mvvm_demo;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kamana.mvvm_demo.databinding.ItemRepoBinding;
import com.example.kamana.mvvm_demo.modal.Repository;
import com.example.kamana.mvvm_demo.viewmodal.ItemRepositoryViewModal;

import java.util.Collections;
import java.util.List;


/**
 * Created by kamana on 14/7/17.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {

    private List<Repository> repositories;

    public RepositoryAdapter(){
        this.repositories = Collections.emptyList();
    }

    public RepositoryAdapter(List<Repository> repositories){
        this.repositories = repositories;
    }

    public void setRepositories(List<Repository> repositories){
        this.repositories = repositories;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemRepoBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext())
                ,R.layout.item_repo
                ,parent
                ,false);

        return new RepositoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        holder.bindRepository(repositories.get(position));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public static class RepositoryViewHolder extends RecyclerView.ViewHolder {

        final ItemRepoBinding binding;

        public RepositoryViewHolder(ItemRepoBinding binding) {
            super(binding.cardView);
            this.binding = binding;
        }

        void bindRepository(Repository repository){
            if(binding.getRepoViewModal() == null){
                binding.setRepoViewModal(new ItemRepositoryViewModal(itemView.getContext(), repository));
            } else {
                binding.getRepoViewModal().setRepository(repository);
            }
        }
    }
}
